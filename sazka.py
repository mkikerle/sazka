import random

def generate_numbers(start, stop, count, sort):
    """Vygeneruje predem znamy pocet nahodnych cisel z rozsahu od-do 

    Parameters
    ----------
    start : int
        Dolni mez rozsahu
    stop : int
        Horni mez rozsahu
    count : int 
        Pocet nahodnych cisel
    sort : bool
        Seradit cisla od nejmensiho po nejvetsi

    Returns
    -------
    list
        Seznam nahodne vygenrovanych cisel
    """
    numbers = []

    while (numbers.__len__() < count):
        number = random.randint(start, stop)

        if (number in numbers):
            continue
        else:
            numbers.append(number)

    if (sort == True):
        numbers.sort()

    return numbers

def print_output(option, count):
    """Vytiskne na vystup vygenerovana cisla

    Parameters
    ----------
    option : str
        Volba loterie, ktera se ma pro tisk pouzit (S - Sportka, E - Eurojackpot)
    count : int
        Pocet sloupcu, ktere se maji u dane loterie vygenerovat

    """
    sort = ""
    while (sort not in ["a", "n"]):        
        sort = input("Seradit cisla od nejmensiho po nejvetsi? [A/N]: ").lower()

    sort = True if sort == "a" else False

    for i in range(1, count + 1):
        if (option == "s"):   
            print("Generuji čísla pro Sportku, sloupec {0}:\t{1}".format(i, generate_numbers(1, 49, 6, sort)))
        elif (option == "e"):
            print("Generuji čísla pro Eurojackpot, sloupec {0}:\t{1} - {2}".format(i,  generate_numbers(1, 50, 5, sort), generate_numbers(1, 10, 2, sort)))            

def main():
    option = ""

    while (option != "k"):

        option = input("SPORTKA [S], EUROJACKPOT [E], KONEC [K]: ").lower()
        count = 0

        if (option == "k"):   #konec
            exit()
        elif (option in ["s", "e"]): #sportka nebo eurojackpot
            while not (count > 0 and count < 11):
                count = input("Počet sloupců (1-10): ")
                if (count.isdigit() == False):
                    count = 0
                else:
                    count = int(count)
            print_output(option, count)
        else:
            print("Neplatná volba!")

if __name__ == "__main__":
    main()